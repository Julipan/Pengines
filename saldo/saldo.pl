:- module(saldo,
	[  sense_to_written_form/2,
	   sense_to_pos/2,
	   sense_to_lemgram/2,
	   sense_to_paradigm/2,
	   sense_relation_primary/2,
	   sense_relation_secondary/2,
	   form_representation/5,
	   written_form/1,
	   get_levenshtein/3,
       get_saldo_all/7
	]).

:- [saldo_fr].
:- [saldo_rel_pri].
:- [saldo_rel_sec].
:- [levenshtein].

% The main predicates loaded above are:
% form_representation(Sense, WrittenForm, POS, Lemgram, Paradigm)
% sense_relation_primary(Sense, PrimarySenseRelation)
% sense_relation_secondary(Sense, SecondarySenseRelation)
% levenshtein(String1, String2, Distance)

get_saldo_all(Sense, WrittenForm, POS, Lemgram, Paradigm, PrimarySenseRelation, SecondarySenseRelation) :-
    form_representation(Sense, WrittenForm, POS, Lemgram, Paradigm),
    sense_relation_primary(Sense, PrimarySenseRelation),
    ( sense_relation_secondary(Sense, Sec)
      -> SecondarySenseRelation = Sec
      ; SecondarySenseRelation = 'none'
    ).

written_form(X) :-
    form_representation(_,X,_,_,_).

sense_to_written_form(X, Y) :-
    form_representation(X,Y,_,_,_).

sense_to_pos(X, Y) :-
    form_representation(X,_,Y,_,_).

sense_to_lemgram(X, Y) :-
    form_representation(X,_,_,Y,_).

sense_to_paradigm(X, Y) :-
    form_representation(X,_,_,_,Y).

levenshtein_saldo(X, Y, Z) :-
    written_form(Y),
    levenshtein(X, Y, Z).

get_head([H|_], H).

begins_with(Letter, Word) :-
    written_form(Word),
    atom_chars(Word, [Letter|_]).

cwwf(IncompleteWord, CompleteWord) :-
    atom_chars(IncompleteWord, X),
    written_form(CompleteWord),
    atom_chars(CompleteWord, Y),
    length(X, L),
    length(Y, L),
    replace(' ',_,X,X2),
    X2 = Y.

replace(_, _, [], []).
replace(O, _, [O|T], [R|T2]) :-
    replace(O, R, T, T2).
replace(O, _, [H|T], [H|T2]) :-
    dif(H, O),
    replace(O, _, T, T2).

word_list(X) :-
    findall(Y, written_form(Y), X).

get_levenshtein(Word, Match, Score) :-
    written_form(Word),
    Match = Word,
    Score = 0.

get_levenshtein(Word, Match, Score) :-
    \+ written_form(Word),
    sorted_score_list(Word, List),
    member(Score-Match, List).

sorted_score_list(Word, List) :-
    word_list(X),
    generate_score_list(Word, X, [], Unsorted),
    keysort(Unsorted, List).

generate_score_list(_, [], Acc, Acc).

generate_score_list(Word, [H|T], Acc, ScoreList) :-
    levenshtein(Word, H, Score),
    generate_score_list(Word, T, [Score-H|Acc], ScoreList).

get_isub(Word, Match, Score) :-
    written_form(Word),
    Match = Word,
    Score = 1.

get_isub(Word, Match, Score) :-
    \+ written_form(Word),
    isub_sorted_score_list(Word, List),
    member(Score-Match, List).

isub_sorted_score_list(Word, List) :-
    word_list(X),
    isub_generate_score_list(Word, X, [], Unsorted),
    keysort(Unsorted, Reverse),
    reverse(Reverse, List).

isub_generate_score_list(_, [], Acc, Acc).

isub_generate_score_list(Word, [H|T], Acc, ScoreList) :-
    isub(Word, H, true, Score),
    isub_generate_score_list(Word, T, [Score-H|Acc], ScoreList).
