/* JavaScript for the Saldo application */

var divnumber = 0;

function prevDiv() {
    if (divnumber == 1) {
        document.getElementById('prevbutton').style.backgroundImage="url('img/leftbutton-inactive.png')";
    } else if (divnumber == 3) {
        document.getElementById('nextbutton').style.backgroundImage="url('img/rightbutton.png')";
    }
    if (divnumber > 0) {
        divnumber -= 1;
        movediv = -342 * divnumber;
        document.getElementById('querybox').style.transform = 'translate(' + movediv + 'px,0)';
        document.getElementById('querybox').style.MozTransform = 'translate(' + movediv + 'px,0)';
        document.getElementById('querybox').style.WebkitTransform = 'translate(' + movediv + 'px,0)';
        document.getElementById('querybox').style.OTransform = 'translate(' + movediv + 'px,0)';
        document.getElementById('querybox').style.MsTransform = 'translate(' + movediv + 'px,0)';
    }
}

function nextDiv() {
    if (divnumber == 2) {
        document.getElementById('nextbutton').style.backgroundImage="url('img/rightbutton-inactive.png')";
    } else if (divnumber == 0) {
        document.getElementById('prevbutton').style.backgroundImage="url('img/leftbutton.png')";
    }
    if (divnumber < 3){
        divnumber += 1;
        movediv = -342 * divnumber;
        document.getElementById('querybox').style.transform = 'translate(' + movediv + 'px,0)';
        document.getElementById('querybox').style.MozTransform = 'translate(' + movediv + 'px,0)';
        document.getElementById('querybox').style.WebkitTransform = 'translate(' + movediv + 'px,0)';
        document.getElementById('querybox').style.OTransform = 'translate(' + movediv + 'px,0)';
        document.getElementById('querybox').style.MsTransform = 'translate(' + movediv + 'px,0)';
    }
}

var pengine;

function ask() {
    // 0 = Saldo info
    // 1 = Levenshtein
    // 2 = Isub
    // 3 = CWWF
    var query = $("#query" + divnumber).val();
    var fullquery;
    switch (divnumber) {
        case 0:
            fullquery = 'get_saldo_all(Sense, ' + query + ', POS, Lemgram, Paradigm, PrimarySenseRelation, SecondarySenseRelation)';
            break;
        case 1:
            fullquery = 'get_levenshtein(' + query + ', Match, Score)';
            break;
        case 2:
            fullquery = 'get_isub(' + query + ', Match, Score)';
            break;
        case 3:
            fullquery = 'cwwf(' + query + ', CompleteWord)';
            break;
    }
    if (query) {
        writeln("You entered <i>" + query + ".<br />");
        pengine = new Pengine({
            application: 'saldo',
            ask: fullquery,
            onsuccess: function() {
                var fullout;
                switch (divnumber) {
                    case 0:
                        var sense = JSON.stringify(this.data[0].Sense);
                        var pos = JSON.stringify(this.data[0].POS);
                        var lemgram = JSON.stringify(this.data[0].Lemgram);
                        var paradigm = JSON.stringify(this.data[0].Paradigm);
                        var pri_rel = JSON.stringify(this.data[0].PrimarySenseRelation);
                        var sec_rel = JSON.stringify(this.data[0].SecondarySenseRelation);
                        fullout = "Sense: " + sense + "<br />" +
                                  "POS: " + pos + "<br />" +
                                  "Lemgram: " + lemgram + "<br />" +
                                  "Paradigm: " + paradigm + "<br />" +
                                  "Primary sense relation: " + pri_rel + "<br />" +
                                  "Secondary sense relation: " + sec_rel + "<br />" +
                                  "----------------------------------";
                        break;
                    case 1:
                        var match = JSON.stringify(this.data[0].Match);
                        var score = JSON.stringify(this.data[0].Score);
                        fullout = match + ": " + score;
                        break;
                    case 2:
                        var match = JSON.stringify(this.data[0].Match);
                        var score = JSON.stringify(this.data[0].Score);
                        fullout = match + ": " + score;
                        break;
                    case 3:
                        var word = JSON.stringify(this.data[0].CompleteWord);
                        fullout = word;
                        break;
                }
                writeln(fullout);
                if (this.more) {
                    disableButtons(true, false, false, false);
                } else {
                    writeln("No more solutions");
                    disableButtons(false, true, true, true);
                }
            },
            onfailure: function() {
                writeln("Failure");
                disableButtons(false, true, true, true);
            },
            onstop: function() {
                writeln("Stopped");
                disableButtons(false, true, true, true);
            },
            onabort: function() {
                writeln("Aborted");
                disableButtons(false, true, true, true);
            },
            onerror: function() {
                writeln("Error: " + this.data);
                disableButtons(false, true, true, true);
            }
        });
    }
}

function next() {
    pengine.next();
}

function stop() {
    pengine.stop();
}

function abort() {
    pengine.abort();
}


function writeln(string) {
    $('#output').append(string + "<br />");
}

function disableButtons(ask, next, stop, abort) {
    $("#ask-btn").prop("disabled", ask);
    $("#next-btn").prop("disabled", next);
    $("#stop-btn").prop("disabled", stop);
    $("#abort-btn").prop("disabled", abort);
}

$(document).ready(function() {
    $("#ask-btn").on("click", ask);
    $("#next-btn").on("click", next);
    $("#stop-btn").on("click", stop);
    $("#abort-btn").on("click", abort);
    $("#assert-btn").on("click", function() {
        update('assert_');
    });
    $("#retract-btn").on("click", function() {
        update('retract_');
    });
    $("#clear-btn").on("click", function() {
        $('#output').html('');
    });
});
