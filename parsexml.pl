:- use_module(library(sgml)).
:- use_module(library(xpath)).

get_form_repr(XMLfile, Sense, WrittenForm, POS, Lemgram, Paradigm) :-
    load_xml_file(XMLfile, XML),
    xpath(XML, //'LexicalEntry', LE),
    xpath(LE, 'Sense'(@id), Sense),
    xpath(LE, 'Lemma'/'FormRepresentation', FR),
    xpath(FR, feat(@att=writtenForm,@val), WrittenForm),
    xpath(FR, feat(@att=partOfSpeech,@val), POS),
    xpath(FR, feat(@att=lemgram,@val), Lemgram),
    xpath(FR, feat(@att=paradigm,@val), Paradigm).

write_form_repr(XMLfile) :-
    open('saldo_fr.pl', write, Stream),
    forall(get_form_repr(XMLfile, Sense, WF, POS, Lemgram, Paradigm),
           format(Stream, "~q.~n", [form_representation(Sense, WF, POS, Lemgram, Paradigm)])),
    close(Stream).

get_sense_relation1(XMLfile, Sense, SenseRelation1) :-
    load_xml_file(XMLfile, XML),
    xpath(XML, //'LexicalEntry', LE),
    xpath(LE, 'Sense'(@id), Sense),
    xpath(LE, 'Sense'/'SenseRelation'(@targets=SenseRelation1)/feat(@val=primary), _).

get_sense_relation2(XMLfile, Sense, SenseRelation2) :-
    load_xml_file(XMLfile, XML),
    xpath(XML, //'LexicalEntry', LE),
    xpath(LE, 'Sense'(@id), Sense),
    xpath(LE, 'Sense'/'SenseRelation'(@targets=SenseRelation2)/feat(@val=secondary), _).

write_sense_relation1(XMLfile) :-
    open('saldo_rel_pri.pl', write, Stream),
    forall(get_sense_relation1(XMLfile, Sense, SR),
           format(Stream, "~q.~n", [sense_relation_primary(Sense, SR)])),
    close(Stream).

write_sense_relation2(XMLfile) :-
    open('saldo_rel_sec.pl', write, Stream),
    forall(get_sense_relation2(XMLfile, Sense, SR),
           format(Stream, "~q.~n", [sense_relation_secondary(Sense, SR)])),
    close(Stream).

write_all(XMLfile) :-
    write_form_repr(XMLfile),
    write_sense_relation1(XMLfile),
    write_sense_relation2(XMLfile).
